part of game;

class ProjectileAnimation {
  Vec2D start, end, current;
  Vec2D tilesize = new Vec2D(16,8);
  Sprite image;
  bool complete = false;
  GameEngine engine;
  Function onComplete;

  int progress = 0;
  num total = 10;

  bool done() {
    return progress > total;
  }

  // Interpolate and advance the position
  void advance() {
    var d = end - start;
    progress++;
    d = d*(progress/total);
    current = new Vec2D.integer(start + d);

    // trigger the onComplete
    if (done()) {
      onComplete();
    }
  }

  void draw(RenderList render) {
    var pos = GameEngine.mapToScreen(current, tilesize) - engine.screenOffset;
    int order = pos.y + image.height;
    render.addSprite(image.image, pos, null, order);
  }

  ProjectileAnimation(this.engine, this.image, this.start, this.end, this.onComplete) {
    current = start.clone();

    total = start.distanceTo(end)*1.5;
  }
}

class GameEngine {

  CanvasElement canvas;
  CanvasRenderingContext2D context;

  TileMap map;
  Vec2D dudepos;
  Vec2D screenOffset;
  Vec2D clientSize;
  bool running = false;

  final Vec2D tileSize = new Vec2D(32,16);

  Map<int,Sprite> sprites = {};
  List<GameEntity> entities = [];

  // Some callbacks
  Function pickEnemy = null;

  String keypresses = "";
  // Create a new render list with a limited refresh
  RenderList render = new RenderList(50);

  var nextPlayerAction = null;
  int currentTurn = 1;
  int nextPlayerTurn = 1;

  GameEntity _player;
  GameEntity get player => _player;
  void set player(GameEntity p)  {
    _player = p;
    dudepos = p.position.clone();
  }

  ProjectileAnimation bullet = null;

  static Vec2D mapToScreen(Vec2D map, Vec2D size) {
    return new Vec2D(map.x*size.x - map.y*size.x, map.x*size.y + map.y*size.y);
  }

  bool canAct() {
    return bullet == null;
  }

  /**
   * Checks to see if a tile can possibly occlude any entities on the map
   */
  bool canOcclude(Vec2D pos) {
    for (int i = 0; i < 3; ++i) {
      for (int j = 0; j < 3; ++j) {
        var p = new Vec2D(pos.x-i, pos.y-j);
        var tile = map.tile(p);

        if (tile != null && tile.entities.length > 0) {
          return true;
        }
      }
    }

    return false;
  }

  /**
   * Create the render list for the game engine
   */
  RenderList getRenderList() {

    if (sprites.isEmpty) {
      return render;
    }

    if (!render.queryRefreshState()) {
      return render;
    }

    render.clear();

    // Draw a diamond from the top right to the bottom left of the screen
    Vec2D tileSize = new Vec2D(16,8);

    int len = 2;
    num diag = sqrt(pow(clientSize.x/2, 2) + pow(clientSize.y/2, 2));
    num tileDiag = sqrt(tileSize.x*tileSize.x + tileSize.y*tileSize.y);
    diag = diag / tileDiag;

    int width = clientSize.x ~/ (tileSize.x) + 1;
    int height = clientSize.y ~/  tileSize.y + 1;
    Vec2D orig = new Vec2D(player.position.x - diag.toInt(), player.position.y);
    Vec2D offset = mapToScreen(orig, tileSize);
    screenOffset = offset;

    Vec2D rowp = orig.clone();
    Set rendered = new Set();

    for (int sy = 0; sy < height; ++sy) {
      Vec2D tp = rowp.clone();

      for (int sx = 0; sx < width; ++sx) {
        var tile = map.tile(tp);
        rendered.add(tile);

        if (tile != null && tile.visibility != 0) {
          int biggestHeight = 0;
          var dp = mapToScreen(tp, tileSize);
          dp.x = dp.x - offset.x;
          dp.y = dp.y - offset.y;

          if (dp.x > -32 && dp.x < 832) {
            for (int l = 0; l < tile.layers.length; ++l) {
              var rp = new Vec2D(dp.x, dp.y);

              var ent = tile.layers[l];
              ImageElement img = sprites[ent.id].image;
              rp.y = rp.y - ent.height;

              if (ent.height > biggestHeight) {
                biggestHeight = ent.height;
              }

              var cmd = render.addSprite(img, rp, null, dp.y + l);

              if (tile.visibility == 1) {
                if (ent.height > 0 && canOcclude(tp)) {
                  cmd.alpha = 0.15;
                }
              } else if (tile.visibility == -1) {
                cmd.image = sprites[6].image;
                cmd.point.y += ent.height;
              }
            }

            if (tile.visibility == 1) {
              for (GameEntity ent in tile.entities) {
                // Add the enemy to the list of visible entities
                render.visibleEntities.add(ent);

                // For now, draw the entities at the top of the tile
                var sprite = sprites[ent.sprite];
                var decp = dp.clone();
                decp.y = dp.y - biggestHeight - sprite.height;

                int myorder = decp.y + biggestHeight+1+sprite.height;
                var cmd = render.addSprite(sprite.image, decp, null, myorder++)..alpha = ent.alpha;

                // If a callback is set for selecting enemy entities, create a pick entry
                if (pickEnemy != null) {
                  render.addPick(cmd, ent, pickEnemy);
                }
                var decp2 = decp.clone();
                decp2.y += sprite.height;

                if (ent.decorator != null) {
                  render.addSprite(ent.decorator.image, decp2, null, decp2.y + myorder);
                }
              }
            }
          }
        }

        tp.x += sx%2;
        tp.y -= (sx+1)%2;
      }

      rowp.x ++;
      rowp.y ++;
    }

    if (bullet != null) {
      bullet.advance();
      bullet.draw(render);

      if (bullet.done()) {
        bullet = null;
      }
    }

    return render;
  }

  /**
   * Loads JSON represented sprites into memory
   */
  void loadSpriteData(JsonObject data) {
    for (JsonObject sprite in data.sprites) {
      if (!sprite.containsKey("height")) {
        sprite.isExtendable = true;
        sprite.height = 0;
      }

      ImageElement img = new ImageElement(src : sprite.path);
      sprites[sprite.id] = new Sprite(img, sprite.height);
    }
  }

  getPlayerAction() {
    if (nextPlayerAction != null) {
      var pAction = nextPlayerAction;
      nextPlayerAction = null;
      return pAction;
    }

    return null;
  }

  void step() {
    // for now, don't progress while animating a projectile
    if (running && bullet == null) {
      if (currentTurn == player.nextTurn) {
        player.nextTurn += player.takeTurn(this);
      } else {
        // Update enemies and stuff here
        for (var ent in entities) {
          if (currentTurn >= ent.nextTurn) {
            ent.nextTurn += ent.takeTurn(this);
          }
        }

        entities.removeWhere((e) => e.live == 0);
        currentTurn++;
      }
    }
  }

  void end() {
    running = false;
  }

  void start() {
    running = true;
  }

  GameEngine(this.canvas, this.clientSize) {
    context = canvas.context2D;

    canvas.onClick.listen((e) {
      var off = canvas.offset;
      num x = e.clientX - canvas.borderEdge.left;
      num y = e.clientY - canvas.borderEdge.top;
      Vec2D pos = new Vec2D(x.toInt(),y.toInt());

      if (render != null) {
        render.processSelect(pos);
      }
    });
  }
}
