part of game;

class MapPath {

  Function heuristic;
  MapPath([this.heuristic = _defaultHeuristic]);

  static _defaultHeuristic(Vec2D node, Vec2D end) {
    // Cut down some speed. Might not be optimal for complex routes
    return pow(node.distanceTo(end), 2);
  }

  /**
   * Returns the node from set list with the lowest corresponding value in the set
   */
  Vec2D getLowestCost(List list, Map set) {
    num lowest = double.MAX_FINITE;
    Vec2D ret = null;

    for (Vec2D node in list) {
      if (set[node] < lowest) {
        ret = node;
        lowest = set[node];
      }
    }

    return ret;
  }

  List constructResult(Map path, Vec2D n) {
    if (path.containsKey(n)) {
      var p = constructResult(path, path[n]);
      p.add(n);
      return p;
    } else {
      return [n];
    }
  }

  var slist = [
               new Vec2D(-1,0),
               new Vec2D(1,0),
               new Vec2D(0,-1),
               new Vec2D(0,1)
               ];

  List siblingFilter() {
    return slist;
  }

  calculate(TileMap map, Vec2D start, Vec2D end, [Function valid = null, limit = null]) {
    var open = [start];
    var closed = [];
    int count = 0;
    Map<Vec2D, num> scores = {};
    Map<Vec2D, num> estimates = {};
    Map<Vec2D, Vec2D> path = {};

    scores[start] = 0;
    estimates[start] = scores[start] + heuristic(start,end);

    while (!open.isEmpty) {
      if (limit != null && count > limit) {
        return [];
      }

      var current = getLowestCost(open, estimates);

      if (current == end) {
        // Return the path
        return constructResult(path, end);
      }

      open.remove(current);
      closed.add(current);

      // Add all the neighbours of current that are not considered to the open set
      var filter = siblingFilter();
      for (Vec2D sf in filter) {
        Vec2D sibling = new Vec2D(current.x+sf.x, current.y+sf.y);

        bool v = true;
        // If do not consider invalid tiles
        if (valid != null) {
          v = valid(map, sibling);
        }

        if (v && !closed.contains(sibling)) {
          num total = scores[current] + 1; //heuristic(current,sibling);

          // If the node has not been added, or if it has been added with
          // a worse score, we update the open set and estimated scores
          if (!open.contains(sibling) || total < scores[sibling]) {
            path[sibling] = current;
            scores[sibling] = total;
            estimates[sibling] = scores[sibling] + heuristic(sibling,end);

            // Make sure it is inserted to the open set
            if (!open.contains(sibling)) {
              open.add(sibling);
            }
          }
        }
      }

      count++;
    }

    return [];
  }
}
