part of game;

/**
 * Because we allways need a vector
 */
class Vec2D {
  List<num> _vec = [0,0];

  Vec2D(num xp, num yp) {
    _vec[0] = xp;
    _vec[1] = yp;
  }

  Vec2D operator+(Vec2D p) {
    return new Vec2D(_vec[0] + p.x, _vec[1] + p.y);
  }

  Vec2D operator-(Vec2D p) {
    return new Vec2D(this.x - p.x, this.y - p.y);
  }

  operator*(num s) {
    return new Vec2D(_vec[0] * s, _vec[1] * s);
  }

  operator/(num s) {
    return new Vec2D(_vec[0]/s, _vec[1]/s);
  }

  bool operator==(Vec2D b) {
    return x == b.x && y == b.y;
  }

  num distanceTo(Vec2D b) {
    return sqrt(pow(b.x - x, 2)+pow(b.y - y,2));
  }

  Vec2D clone() {
    return new Vec2D(x, y);
  }

  num get length {
    return sqrt(pow(x, 2) + pow(y,2));
  }

  String toString() => "${x},${y}";
  num get hashCode {
    return toString().hashCode;
  }

  num get x => _vec[0];
  num get y => _vec[1];
  set x(num v) => _vec[0] = v;
  set y(num v) => _vec[1] = v;

  // Create an integer vector
  factory Vec2D.integer(Vec2D v) {
    return new Vec2D(v.x.toInt(), v.y.toInt());
  }

  void normalize() {
    if (length == 0) {
      throw "Invalid vector";
    }

    x = x / length;
    y = y / length;
  }
}

class Rect {
  Vec2D position;
  Vec2D size;

  Rect(this.position, this.size);

  bool contains(Vec2D p) {
    return p.x >= position.x && p.y >= position.y &&
        p.x < position.x + size.x && p.y < position.y + size.y;
  }

  num get left => position.x;
  num get top => position.y;
  num get right  => position.x + size.x;
  num get bottom => position.y + size.y;
  num get width => size.x;
  num get height => size.y;
  num get area => size.x * size.y;
}
