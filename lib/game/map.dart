part of game;

class Entry {
  int id;
  int height;
  Entry(this.id, this.height);
}

class Tile {
  List<Entry> layers = [];
  List<GameEntity> entities = [];

  bool passable = false;
  bool sightblock = true;
  int type = 0;
  int visibility = 0;

  void addLayer(int id, int height) {
    layers.add(new Entry(id,height));
  }

  void setLayer(int layer, int id, int height) {
    if (layers.length <= layer) {
      addLayer(id, height);
    } else {
      layers[layer].id = id;
      layers[layer].height = height;
    }
  }

  Tile([int l0 = null]) {
    if (l0 != null) {
      layers.add(new Entry(l0,0));
    }
  }
}

class TileMap {

  List<List<Tile>> map = [];
  Vec2D escapePos;

  bool passableLimit(TileMap m, Vec2D pos) {
    var t = m.tile(pos);
    return t != null && t.passable;
  }

  /**
   * Calculates the visible area of the map from the players position
   */
  void calculatePlayerVision(Vec2D pos) {
    Vec2D ul = pos.clone() - new Vec2D(20,20);
    Vec2D br = pos.clone() + new Vec2D(20,20);

    num viewDistance = 10;

    for (int y = ul.y; y < br.y; ++y) {
      for (int x = ul.x; x < br.x; ++x) {
        Vec2D cur = new Vec2D(x,y);
        var t = tile(cur);

        if (t != null) {
          if (t.visibility != 0) {
            t.visibility = -1;
          } else {
            t.visibility = 0;
          }
        }
      }
    }

    Function lighter = (p) {
      var t = tile(p);

      if (t != null) {
        t.visibility = 1;

        if (t.sightblock) {
          return false;
        }

        return true;
      }

      return false;
    };

    for (num d = 0; d <= 2*PI; d += 0.02) {
      num x = cos(d)*viewDistance + pos.x;
      num y = sin(d)*viewDistance + pos.y;

      Vec2D tp = new Vec2D(x.round(), y.round());
      // Light the path
      runLine(pos, tp, lighter);
    }
  }

  /**
   * See if there is LOS between two given points
   */
  bool hasLos(Vec2D from, Vec2D to) {
    bool block = false;
    var cb = (tp) {
      var tl = tile(tp);

      if (tl.sightblock) {
        block = true;
      }

      return !block;
    };

    // Run the function over a line in the map
    runLine(from, to, cb);

    return !block;
  }

  void runLine(Vec2D from, Vec2D to, Function callback) {
    int x0 = from.x;
    int x1 = to.x;
    int y0 = from.y;
    int y1 = to.y;

    bool steep = (y1-y0).abs() > (x1-x0).abs();

    if (steep) {
      int tx0 = x0;
      x0 = y0;
      y0 = tx0;

      int tx1 = x1;
      x1 = y1;
      y1 = tx1;
    }

    int step = 1;
    if (x0 > x1) {
      step = -1;
    }

    num grad = (y1-y0) / (x1-x0);
    num y = y0;
    for (int x = x0; x != x1; x += step) {
      var tp = new Vec2D(x.toInt(),y.round());

      if (steep) {
        tp = new Vec2D(y.toInt(), x.toInt());
      }

      if (!callback(tp)) {
        break;
      }

      y += grad*step;
    }
  }

  Tile tile(Vec2D pos) {
    Tile ret = null;

    if (pos.y >= 0 && pos.y < map.length) {
      if (pos.x >= 0 && pos.x < map[0].length) {
        ret = map[pos.y][pos.x];
      }
    }

    return ret;
  }

  TileMap(int w, int h) {
    Random rng = new Random();

    for (int i = 0; i < h; ++i) {
      List empty = [];

      for (int j = 0; j < w; ++j) {
        empty.add(new Tile());
      }

      map.add(empty);
    }
  }
}
