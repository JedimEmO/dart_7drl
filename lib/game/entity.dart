part of game;

abstract class GameEntity {
  Vec2D position;
  int sprite = 42;
  int nextTurn = 1;
  Sprite decorator;
  num alpha = 1.0;
  int live = 1;

  GameEntity(this.position);

  /**
   * Take a game turn.
   *
   *    returns the number of game ticks of cooldown the performed action has
   */
  int takeTurn(GameEngine e);

  /**
   * Moves the entity between map tiles
   */
  void moveTo(TileMap map, Vec2D to) {
    var target = map.tile(to);

    if (target == null || !target.passable) {
      return;
    }

    map.tile(position).entities.remove(this);
    target.entities.add(this);
    position = to.clone();
  }
}
