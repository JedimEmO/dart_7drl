part of game;

class Sprite {
  int height;
  ImageElement image;

  Sprite(this.image, this.height);
}

/**
 * Represents the action of rendering something to the screen at a given position.
 */
class RenderCommand {
  ImageElement image;
  Vec2D point;
  int order = 0;
  num alpha = 1.0;
  List hue;
  Function selectCallback = null;

  RenderCommand(this.image, this.point);

  void render(CanvasRenderingContext2D context) {
    context.drawImage(image, point.x, point.y);
  }

  /**
   * Applies any context state properties requried for this command
   */
  void applyState(CanvasRenderingContext2D context) {
    context.globalAlpha = alpha;
  }
}

class PickLink {
  var entity;
  RenderCommand command;

  bool processClick(Vec2D pos) {
    Rect r = new Rect(command.point, new Vec2D(command.image.width, command.image.height));

    // Coarse check
    if (r.contains(pos.clone())) {

      var img = command.image;
      var ctx = new CanvasElement();
      ctx.context2D.drawImage(img, 0, 0);
      var np = new Vec2D(pos.x - r.left, pos.y - r.top);

      if (ctx.context2D.getImageData(np.x, np.y, 1, 1).data[0] > 0) {
        command.selectCallback(entity);
        return true;
      }
    }

    return false;
  }

  PickLink(this.entity, this.command);
}

/**
 * Collection of render commands to be sent to the view for rendering.
 *
 * Also contains screen picking information
 */
class RenderList {
  List<RenderCommand> list = [];
  List<PickLink> pickList = [];
  List<GameEntity> visibleEntities = [];

  num lastUpdate  = 0;
  num rate = 10;

  /**
   * Returns true if the render list should be refreshed by the engine. This
   * means that the time since the last refresh is greater than the specified rate.
   */
  bool queryRefreshState() {
    num now = new DateTime.now().millisecondsSinceEpoch;
    bool update = false;

    if (now - lastUpdate > rate) {
      lastUpdate = now;
      update = true;
    }

    return update;
  }

  void addPick(RenderCommand cmd, GameEntity ent, Function callback) {
    cmd.selectCallback = callback;
    pickList.add(new PickLink(ent,cmd));
  }

  void processSelect(Vec2D pos) {
    for (var e in pickList) {
      if (e.processClick(pos)) {
        return;
      }
    }
  }

  void pruneVisible() {
    visibleEntities.removeWhere((e) => e.live == 0);
  }

  void clear() {
    list.clear();
    pickList.clear();
    visibleEntities.clear();
  }

  /**
   * Adds a sprite render command to the list. If pick is non-null, it will
   * be called upon picking the sprite from screen coordinates.
   */
  RenderCommand addSprite(ImageElement img, Vec2D pos, [Function pick = null, num order = null, num alpha = 1.0]) {
    var added = new RenderCommand(img, pos);

    if (pick != null) {
      // maybe do something here
    }

    if (order != null) {
      added.order = order;
    }

    added.alpha = alpha;
    list.add(added);
    return added;
  }

  void sort() {
    list.sort((e,f) => e.order - f.order);
  }

  RenderList(this.rate);
}

/**
 * Polls the game engine for a list of renderables, and takes care of placing them on the screen.
 *
 * Works on the canvas for now, could potentially be replaced with faster rendering.
 */
class GameView {
  CanvasRenderingContext2D context;
  GameEngine engine;
  num lastDraw = 0;
  num fps = 0;

  Random rng = new Random();

  GameView(this.engine) {
    context = engine.context;
  }

  void renderFrame(GameEngine e) {
    // Calculate our average FPS
    num time = new DateTime.now().millisecondsSinceEpoch;
    fps = (1000/(time - lastDraw))*0.05 + fps * 0.95;
    lastDraw = time;

    context.setFillColorRgb(100, 100, 100, 1);
    context.fillRect(0, 0, context.canvas.width, context.canvas.height);

    var list = e.getRenderList();
    list.sort();

    // Execute all the render commands
    for (RenderCommand c in list.list) {
      context.save();
      c.applyState(context);
      c.render(context);
      context.restore();
    }

    // Perform logic & shit
    e.step();

    // Request the next animation frame from the browser world
    requestRender();
  }

  /**
   * Request the next render of the view
   */
  void requestRender() {
    window.requestAnimationFrame((num _) {renderFrame(engine); });
  }
}
