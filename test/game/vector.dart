part of game_test;

testVectorBasics() {
  Vec2D vec = new Vec2D(1,1);
  Vec2D add = new Vec2D(2,2);

  expect(vec.x, equals(1));
  expect(vec.y, equals(1));
  expect(vec.distanceTo(add), equals(sqrt(2)));
  expect((vec + add).x, equals(3));
  expect((vec - add).y, equals(-1));

  Vec2D mult = new Vec2D(1,2);
  mult = mult * 10;

  expect(mult.x, equals(10));
  expect(mult.y, equals(20));


  Vec2D copy = mult.clone();
  copy.x = 0;
  copy.y = 0;

  expect(copy.x, isNot(equals(mult.x)));
}

testRectBasics() {

  Vec2D pos = new Vec2D(1,2);
  Vec2D size = new Vec2D(10,11);
  Rect r = new Rect(pos, size);

  expect(r.left, equals(1));
  expect(r.top, equals(2));
  expect(r.right, equals(11));
  expect(r.bottom, equals(13));
  expect(r.width, equals(10));
  expect(r.height, equals(11));

  Vec2D point = new Vec2D(10,10);
  Vec2D s2 = new Vec2D(20,30);
  Rect r2 = new Rect(point,s2);
  Vec2D p1 = new Vec2D(11,10);

  expect(r2.contains(p1), equals(true));
}