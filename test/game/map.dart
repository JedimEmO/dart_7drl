part of game_test;

testPathing() {

  Vec2D size = new Vec2D(50,50);
  TileMap map = new TileMap(size.x,size.y);
  MapPath p = new MapPath();
  Random rng = new Random();

  // Try resolving paths between random points of the map
  int tests = 100;
  for (int i = 0; i < tests; ++i) {
    Vec2D start = new Vec2D(rng.nextInt(size.x), rng.nextInt(size.y));
    Vec2D end = new Vec2D(rng.nextInt(size.x), rng.nextInt(size.y));
    var path = p.calculate(map, start, end);

    expect(path, contains(start));
  }
}

