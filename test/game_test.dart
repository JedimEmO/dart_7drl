library game_test;

import "package:unittest/unittest.dart";
import "package:dart_7drl/game.dart";
import "dart:math";

part "game/map.dart";
part "game/vector.dart";

void main() {
  group("math", () {
    group("Rect", () {
      test("Basic Rect functionality", () {
        testRectBasics();
      });
    });

    group("Vec2D", () {
      test("Basic vector functionality", () {
        testVectorBasics();
      });
    });
  });

  group("map", () {
    test("Pathfinding", () {
      testPathing();
    });
  });
}