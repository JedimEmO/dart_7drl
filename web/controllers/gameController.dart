part of dart_7drl;

@NgController(
    selector: "[game-controller]",
    publishAs: "g"
    )
class GameController {

  List content = [];
  String title = "DartRL";
  num fps = 0;
  bool loading = false;
  int level = -2;
  String message = "You failed";

  GameEngine engine;
  GameView view;
  GameEntity player, selected;


  again() {
    loading = true;
    newGame();
  }

  /**
   * Callback used to pick an enemy from the screen for attacking.
   */

  unselect() {
    if (selected != null) {
      selected.decorator = null;
      selected = null;
    }
  }

  Enemy getNextEnemy(Enemy ent) {
    var list = [];
    engine.render.pruneVisible();

    for (var e in engine.render.visibleEntities) {
      if (e is Enemy) {
        list.add(e);
      }
    }

    if (list.isEmpty) {
      return null;
    }

    int idx = 0;
    list.sort((a,b) => a.position.distanceTo(player.position) - b.position.distanceTo(player.position));

    if (ent != null && list.contains(ent)) {
     idx = list.indexOf(ent);
    }

    if (list.isNotEmpty) {
      var e = list[(idx+1)%list.length];
      return e;
    }

    return null;
  }

  selectEnemy(GameEntity enemy) {
    unselect();

    if (enemy is Enemy) {
      selected = enemy;
      enemy.decorator = engine.sprites[7];
    }
  }

  void attackSelected() {
    if (selected != null && engine.canAct()) {
      var pp = engine.player.position;
      var ep = selected.position;

      if (engine.map.hasLos(pp.clone(),  ep.clone())) {

        engine.bullet = new ProjectileAnimation(engine, engine.sprites[8], pp, ep, () {
          engine.nextPlayerAction = new Attack(selected);
          unselect();
        });
      } else {
        unselect();
      }
    }
  }

  void smokeSelected() {
    if (selected != null && engine.canAct()) {
      var pp = engine.player.position;
      var ep = selected.position;

      if (engine.map.hasLos(pp.clone(),  ep.clone())) {
        engine.bullet = new ProjectileAnimation(engine, engine.sprites[8], pp, ep, () {
          engine.nextPlayerAction = new SmokeTarget(engine, ep);
          unselect();
       });
      } else {
        unselect();
      }
    }
  }

  void win() {
    engine.running = false;
    message = "Congratulations, you won!";
  }

  void cycleSelect() {
    selectEnemy(getNextEnemy(selected));
  }

  void movePlayer(int x, int y) {
    Vec2D delt = new Vec2D(x,y);
    var pos = player.position.clone() + delt;

    if (engine.map.escapePos == pos) {
      level++;

      if (level == 0) {
        win();
      } else {
        engine.entities.clear();
        engine.render.visibleEntities.clear();
        var gen = generateMap();
        player.position = gen.playerStart;
        player.moveTo(engine.map, player.position);
        engine.player = player;
        engine.map.calculatePlayerVision(player.position);
      }
    } else {
      var t = engine.map.tile(pos);

      if (t != null && t.passable) {
        engine.nextPlayerAction = new Move(pos);
      }
    }
  }

  handleKeys(e) {
    engine.keypresses += e.charCode.toString() + ",";
    int code = e.charCode;
    var p = engine.dudepos.clone();
    bool moved = false;

    if (engine.canAct()) {
      switch (code) {
        case 55:
          movePlayer(-1,0);
          break;
        case 51:
          movePlayer(1,0);
          break;
        case 57:
          movePlayer(0,-1);
          break;
        case 49:
          movePlayer(0,1);
          break;
        case 115:
          smokeSelected();
          break;
        case 42:
          cycleSelect();
          break;
        case 102:
          attackSelected();
          break;

        case 63:
          help(!showHelp);
          break;
      }
    }
  }

  LevelGenerator generateMap() {
    // Generate a new map
     LevelGenerator gen = new LevelGenerator(new Vec2D(100,100));
     gen.roomLimit = 10;
     gen.roomSizeMin = 5;

     Random rng = new Random();
     engine.map = gen.generate(rng.nextInt(100), engine);
     return gen;
  }

  bool showHelp = false;

  void help(bool state) {
    showHelp = state;
  }

  void newGame() {
    loading = true;
    engine = null;
    player = null;
    view = null;

    level = -2;

    CanvasElement canvas = querySelector("#gameWindow");
    engine = new GameEngine(canvas, new Vec2D(canvas.width, canvas.height));

    HttpRequest.getString("images/sprites.json").then((data) {
      var sprites = new JsonObject.fromJsonString(data);
      engine.loadSpriteData(sprites);
    });


    var gen = generateMap();
    player = new Player(gen.playerStart);
    engine.player = player;
    player.moveTo(engine.map, player.position);
    engine.map.calculatePlayerVision(player.position);

    view = new GameView(engine);
    view.renderFrame(engine);
    engine.start();
    loading = false;
  }

  GameController() {
    newGame();
    window.onKeyPress.listen(handleKeys);
  }
}
