part of dart_7drl;

class Move {
  Vec2D target;
  Move(this.target);
}

class Attack {
  GameEntity target;
  Attack(this.target);
}

class SmokeTarget {

  Vec2D target;
  GameEngine e;

  apply() {

    for (int i = -1; i < 2; ++i) {
      for (int j = -1; j < 2; ++j) {
        if (j.abs() + i.abs() == 1) {
          var t = e.map.tile(target + new Vec2D(i,j));

          if (t != null && t.passable) {
            var sent = new Smoke(target + new Vec2D(i,j), 15);
            t.entities.add(sent);
            e.entities.add(sent);
          }
        }
      }
    }
  }

  SmokeTarget(this.e, this.target);
}

class Player extends GameEntity {
  int damageCost = 3;
  int smokeCost = 3;
  int moveCost = 2;

  Player(Vec2D position) : super(position);

  int takeTurn(GameEngine e) {
    int ret = 0;
    var action = e.getPlayerAction();

    if (action != null) {
      if (action is Move) {
        e.dudepos = action.target;
        moveTo(e.map, action.target);
        e.map.calculatePlayerVision(position);
        ret = moveCost;
      } else if (action is Attack) {
        var t = e.map.tile(action.target.position);
        action.target.live = 0;
        t.entities.remove(action.target);
        e.entities.remove(action.target);
        ret = damageCost;
      } else if (action is SmokeTarget) {
        action.apply();
        ret = smokeCost;
      }
    }

    return ret;
  }
}
