part of dart_7drl;

class Room {

  List<List<Vec2D>> connectors = [];
  Rect rect;

  Room(this.rect);

  List<Vec2D> getConnector() {
    return connectors.removeAt(0);
  }

  bool contains(Vec2D pos) {
    return rect.contains(pos);
  }

  Vec2D getRandomTile(Random rng) {
    return new Vec2D(rect.left + rng.nextInt(rect.width-2)+2,
        rect.top + rng.nextInt(rect.height-2)+2);
  }

  /**
   * Generates the connector points of the room
   */
  List generateConnectors(Random rng, int num) {
    int generated = 0;
    int conLen = 5;

    List available = [];

    // Create the list of accessible points
    for (int y = rect.top + 1; y < rect.bottom; ++y) {
      var l = [];
      var r = [];

      for (int i = 0; i < conLen; ++i) {
        l.add(new Vec2D(rect.left - i, y));
        r.add(new Vec2D(rect.right + i, y));
      }

      available.add(l);
      available.add(r);
    }

    for (int x = rect.left + 1; x < rect.right; ++x) {
      var t = [];
      var b = [];

      for (int i = 0; i < conLen; ++i) {
        t.add(new Vec2D(x, rect.top - i));
        b.add(new Vec2D(x, rect.bottom + i));
      }

      available.add(t);
      available.add(b);
    }

    available.shuffle(rng);

    while (generated < num && available.isNotEmpty) {
      connectors.add(available.removeAt(0));
      generated++;
    }

    return connectors;
  }
}

class LevelGenerator {
  Vec2D size;
  Vec2D playerStart, escapePos;
  Random rng;

  // Some properties for the map
  int roomLimit = 1000;
  int roomSizeMin = 5;
  int roomSizeMax = 20;
  int spacing = 8;
  List wallSprites = [3,10];
  int wallSprite;
  int stairSprite = 11;

  // These are seeds known to be rather quick
  List whitelist = [4,
                    12,
                    37,
                    20,
                    46,
                    26,
                    29,
                    48,
                    32,
                    36,
                    22,
                    44,
                    34,
                    5,
                    27,
                    28,
                    24,
                    1,
                    23,
                    42,
                    11,
                    19,
                    16,
                    0];

  /**
   * Generates a game level. */
  LevelGenerator(this.size);

  /**
   * Verifies that a point lies within the map
   */
  bool isValid(Vec2D pos) {
    if (pos == null) {
      return false;
    }

    if (pos.x < 0 || pos.x >= size.x
        || pos.y < 0 || pos.y >= size.y) {
      return false;
    }

    return true;
  }

  bool isValidCoords(int x, int y) {
    return isValid(new Vec2D(x,y));
  }

  bool setTile(TileMap map, int x, int y, int layer, int value, [int height = 0, int type = 0, bool block = false]) {
    bool ret = false;

    if (isValidCoords(x,y)) {
      var tile = map.tile(new Vec2D(x,y));
      tile.setLayer(layer, value, height);
      tile.type = type;
      tile.sightblock = block;

      if (value == 1) {
        tile.passable = true;
      }

      ret = true;
    }

    return ret;
  }

  /**
   * Inserts a room into the tilemap
   */
  Room insertRoom(TileMap map, Rect rect) {
    Vec2D pos = rect.position;
    Vec2D roomSize = rect.size;
    Room room = new Room(rect);

    for (int j = 0; j < roomSize.y; ++j) {
      for (int i = 0; i < roomSize.x; ++i) {
        Vec2D tp = new Vec2D(pos.x + i, pos.y + j);
        setTile(map, tp.x, tp.y, 0, 1);
      }
    }

    return room;
  }

  bool hasGroundNeighbour(TileMap map, Vec2D pos) {
    for (int j = -1; j < 2; ++j) {
      for (int i = -1; i < 2; ++i) {
        Vec2D p = new Vec2D(pos.x + i, pos.y + j);
        var tile = map.tile(p);

        if (tile != null && tile.layers.isNotEmpty && tile.layers[0].id == 1) {
          return true;
        }
      }
    }

    return false;
  }

  void insertWalls(TileMap map) {
    for (int y = 0; y < size.y; ++y) {
      for (int x = 0; x < size.x; ++x) {
        Vec2D pos = new Vec2D(x,y);

        if (hasGroundNeighbour(map, pos)) {
          var tile = map.tile(pos);

          // hack in some walls for now
          if (tile != null && tile.layers.isEmpty) {
            setTile(map,x,y,0,2,0);
            setTile(map,x,y,0,wallSprite,16);
            tile.sightblock = true;
          }
        }
      }
    }
  }

  bool hasSpace(TileMap map, Vec2D pos) {
    for (int j = -spacing~/2; j < spacing~/2+1; ++j) {
      for (int i = -spacing~/2; i < spacing~/2+1; ++i) {
        var tile = map.tile(new Vec2D(pos.x + i, pos.y + j));

        if (tile == null || tile.layers.isNotEmpty) {
          return false;
        }
      }
    }

    return true;
  }

  bool fits(TileMap map, Rect r) {
    for (int y = r.top; y < r.bottom; ++y) {
      for (int x = r.left; x < r.right; ++x) {
        if (!hasSpace(map,new Vec2D(x,y))) {
          return false;
        }
      }
    }

    return true;
  }

  bool fitRoom(TileMap map, Rect r) {
    bool fit = false;
    int tries = 0;

    // Attempt to fit the room
    while (!fit && tries < 100) {
      Vec2D pos = new Vec2D(rng.nextInt(size.x-r.width), rng.nextInt(size.y-r.height));
      r.position = pos;
      Vec2D corner = new Vec2D(r.right, r.bottom);

      var srcTile = map.tile(pos);
      var stopTile = map.tile(corner);

      if (srcTile != null && stopTile != null) {
        fit = fits(map, r);
      }

      tries++;
    }

    return fit;
  }

  Rect getRandomRect(int min, int max) {
    return new Rect(new Vec2D(0,0), new Vec2D(min + rng.nextInt(max-min), min + rng.nextInt(max-min)));
  }

  void populateMonsters(TileMap map, GameEngine e, List<Room> rooms) {

    for (var room in rooms) {
      num count = room.rect.area * rng.nextInt(16)/100;

      for (int i = 0; i < count; ++i) {

        Vec2D pos = new Vec2D(room.rect.left + rng.nextInt(room.rect.width-2)+1,
            room.rect.top + rng.nextInt(room.rect.height-2)+1);
        var tile = map.tile(pos);

        // Don't spawn the player on top of monsters
        if (tile != null && pos.distanceTo(playerStart) > 10 && tile.entities.isEmpty) {
          var theEnemy = new Enemy(pos);
          e.entities.add(theEnemy);
          tile.entities.add(theEnemy);
        }
      }
    }
  }

  // For map generation, use a special heuristic function
  num mapGenHeuristic(Vec2D pos, Vec2D end) {
    var d = end - pos;
    return d.x.abs() + d.y.abs();
  }

  void placeEscape(TileMap map, List rooms) {
    MapPath finder = new MapPath(mapGenHeuristic);
    int sanity = 1000;

    while (escapePos == null) {

      int idx = rng.nextInt(rooms.length);
      Room room = rooms[idx];
      var pos = room.getRandomTile(rng);
      List path = finder.calculate(map, playerStart, pos, (TileMap map, Vec2D pos) {
        var tile = map.tile(pos);

        if (tile == null) {
          return false;
        }

        return tile.passable;
      }, 10000);

      if (!room.contains(playerStart) && path.isNotEmpty) {
        escapePos = pos;
        map.tile(escapePos).setLayer(0, stairSprite, 16);
        map.escapePos = escapePos;
        return;
      }

      --sanity;
      if (sanity == 0) {
        throw "FUCK - ERROR";
      }
    }
  }

  TileMap generate(num seed, GameEngine e, [bool wl = true]) {

    // Hack and fake some performance
    int realSeed = seed;

    if (wl) {
      Random r = new Random(seed);
      realSeed = whitelist[r.nextInt(whitelist.length)];
    }

    rng = new Random(realSeed);

    bool success = false;

    while (!success) {
      try {
        if (playerStart != null) {
          playerStart = null;
        }

        TileMap map = new TileMap(size.x, size.y);
        List rooms = [];
        wallSprite = wallSprites[rng.nextInt(wallSprites.length)];

        // Create some rooms

        for (num room = 0; room < roomLimit; ++room) {

          // Create the position and size of the room
          Vec2D start = new Vec2D(0, 0);
          Rect r = getRandomRect(roomSizeMin, roomSizeMax);

          // Fit the room into the map
          while (!fitRoom(map, r)) {
            r = getRandomRect(roomSizeMin, roomSizeMax);
          }

          // Place the player somewhere
          while (!isValid(playerStart)) {
            playerStart = new Vec2D(r.position.x + 1 + rng.nextInt(r.width-2),
                r.position.y + 1 + rng.nextInt(r.height-2));
          }

          rooms.add(insertRoom(map, r));
        }

        // Generate the connector points of the rooms

        for (Room r in rooms) {
          r.generateConnectors(rng, 1 + rng.nextInt(2));
        }

        populateMonsters(map, e, rooms);

        // Generate the paths between rooms. When a room is out of connectors, remove it from the pool
        MapPath finder = new MapPath(mapGenHeuristic);

        List roomcopy = [];

        for (var room in rooms) {
          roomcopy.add(room);
        }

        int sanity = rooms.length * 1000;

        //TODO: Make sure that all rooms are connected

        while (rooms.isNotEmpty) {
          rooms.shuffle(rng);
          Room top = rooms[0];
          List<Vec2D> start = top.getConnector();

          if (rooms.length < 2) {
            break;
          }

          Room to = rooms[1];
          List<Vec2D> end = to.getConnector();

          var path = finder.calculate(map, start.last, end.last, (TileMap map, Vec2D point) {
            var tile = map.tile(point);

            if (tile != null && tile.layers.isNotEmpty && tile.type == 0) {
              return false;
            }

            return true;
          }, start.last.distanceTo(end.last) * 500);

          if (path.length == 0) {
            throw "Unable to connect map";
          }
          // Insert the path
          for (Vec2D point in path) {
            setTile(map, point.x, point.y, 0, 1, 0, 1);
          }

          // Inser the connectors
          for (int c = 0; c < start.length; ++c) {
            setTile(map, start[c].x, start[c].y, 0, 1, 0, 1);
            setTile(map, end[c].x, end[c].y, 0, 1, 0, 1);
          }

          if (to.connectors.isEmpty) {
            rooms.remove(to);
          }

          if (top.connectors.isEmpty) {
            rooms.remove(top);
          }

          if (--sanity == 0) {
            throw "Failed map generation.";
          };
        }

        insertWalls(map);
        placeEscape(map, roomcopy);

        return map;
      } catch(e) {

      }
    }

    return null;
  }
}
