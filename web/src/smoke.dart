part of dart_7drl;

class Smoke extends GameEntity {

  int life;

  Smoke(Vec2D pos, this.life) : super(pos) {
    sprite = 9;
    alpha = 0.3;
  }

  int takeTurn(GameEngine e) {
    life--;
    var tile = e.map.tile(position);
    tile.sightblock = true;

    e.map.calculatePlayerVision(e.player.position);

    if (life == 0) {
      this.live = 0;
      tile.sightblock = false;
      tile.entities.remove(this);
    }

    return 1;
  }
}
