part of dart_7drl;

class Enemy extends GameEntity {
  // The current path to the player
  List<Vec2D> pathToPlayer = [];
  var player;
  bool seenPlayer = false;

  Enemy(Vec2D position) : super(position) {
    sprite = 5;
  }

  bool enemyValid(TileMap map, Vec2D pos) {
    var tile = map.tile(pos);

    if (tile == null) {
      return false;
    }

    if (tile.entities.contains(this) || tile.entities.contains(player)) {
      return true;
    }

    return tile.passable && tile.entities.isEmpty;
  }

  bool canSee(TileMap map,Vec2D pos) {
    if (seenPlayer || map.hasLos(position, pos)) {
      seenPlayer = true;
    }

    return seenPlayer;
  }

  int takeTurn(GameEngine e) {
    int ret = 1;
    player = e.player;

    // Don't act from across the world
    if (position.distanceTo(player.position) < 10 && canSee(e.map, player.position)) {
      // Avoid needless pathfinding
      if (!pathToPlayer.contains(player.position)) {
        var finder = new MapPath();
        pathToPlayer = finder.calculate(e.map, position, player.position, enemyValid, 30);
      }

      // See if the path towards the player is blocked
      var i = 0;
      for (Vec2D p in pathToPlayer) {
        var tile = e.map.tile(p);

        // If the
        if (tile.entities.isNotEmpty && !tile.entities.contains(this) && !tile.entities.contains(player)) {
          pathToPlayer.removeRange(i+1, pathToPlayer.length);
          break;
        }

        ++i;
      }

      if (pathToPlayer.length >= 2) {
        var next = pathToPlayer.removeAt(1);
        moveTo(e.map, next);
        ret = 2;
      }

      if (position.distanceTo(player.position) < 1) {
        e.end();
      }
    }

    return ret;
  }
}
