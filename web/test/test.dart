library game_testing;

import "dart:html";
import "package:unittest/unittest.dart";
import "../../lib/game.dart";
import "../main.dart";

testGenerator() {
  int total = 100;
  int count = 0;

  for (int i = 0; i < total; ++i ) {
    CanvasElement c = new CanvasElement();
    GameEngine e = new GameEngine(c, new Vec2D(c.width, c.height));
    print("Generating map");
    LevelGenerator gen = new LevelGenerator(new Vec2D(100,100));
    gen.roomLimit = 10;
    gen.roomSizeMin = 5;
    gen.generate(i, e);
    count++;
    print(count);
  }

  expect(count, equals(total));
}

void main() {
  group("Level generator", () {
    test("Generate a ton of maps", () {
      testGenerator();
    });
  });
}