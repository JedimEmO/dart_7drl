library seed_generator;

import "dart:html";
import "../../lib/game.dart";
import "../main.dart";

generate() {
  int total = 50;
  int count = 0;
  List seeds = [];

  for (int i = 0; i < total; ++i ) {
    CanvasElement c = new CanvasElement();
    GameEngine e = new GameEngine(c, new Vec2D(c.width, c.height));
    print("Generating map $count");

    LevelGenerator gen = new LevelGenerator(new Vec2D(100,100));
    gen.roomLimit = 10;
    gen.roomSizeMin = 5;
    int start = new DateTime.now().millisecondsSinceEpoch;
    gen.generate(i, e, false);
    int end = new DateTime.now().millisecondsSinceEpoch;
    seeds.add({"seed": i, "time": (end-start)});
    count++;
  }

  // Now get the fastest seeds:
  seeds.sort((a,b) => a["time"] - b["time"]);

  for (var l in seeds) {
    print(l);
  }
  for (var e in seeds) {
    print ("${e["seed"]},");
  }
}

void main() {
  generate();
}