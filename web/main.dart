library dart_7drl;

import "dart:html";
import "dart:math";

import "package:json_object/json_object.dart";
import "package:angular/angular.dart";
import "../lib/game.dart";

part "controllers/gameController.dart";
part "modules/gameModule.dart";
part "src/levels/generator.dart";
part "src/player.dart";
part "src/enemy.dart";
part "src/smoke.dart";

void main() {
  ngBootstrap(module: new GameModule());
}
